package com.zuitt.wdc044_s01.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="users")
public class User {
    @Id
    @GeneratedValue // this is auto-incrementation
    private Long id;

    @Column
    private String username;

    @Column
    private String password;

    // Constructors
    public User(){}

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    // getters and setters

    public Long getId(){
        return id;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setContent(String password) {
        this.password = password;
    }

    // s04
    @OneToMany(mappedBy = "user")
    @JsonIgnore
    private Set<Post> posts;

    public Set<Post> getPost(){
        return posts;
    }
}
