package com.zuitt.wdc044_s01.repositories;

import org.springframework.data.repository.CrudRepository;
import com.zuitt.wdc044_s01.models.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository <User, Object> {
    User findByUsername(String username); // custom method for finding a user by their username. This will be used in creating JWT
}
