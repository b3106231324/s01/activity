package com.zuitt.wdc044_s01.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.zuitt.wdc044_s01.models.Post;

@Repository
public interface PostRepository extends CrudRepository <Post, Object> {

}
