package com.zuitt.wdc044_s01.services;

import com.zuitt.wdc044_s01.config.JwtToken;
import com.zuitt.wdc044_s01.models.Post;
import com.zuitt.wdc044_s01.models.User;
import com.zuitt.wdc044_s01.repositories.PostRepository;
import com.zuitt.wdc044_s01.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class PostServiceImpl implements PostService {
    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;
    // creating a post
    public void createPost(String stringToken, Post posts) {
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        Post newPost = new Post();
        newPost.setTitle(posts.getTitle());
        newPost.setContent(posts.getContent());
        newPost.setUser(author);
        postRepository.save(newPost);
    }

    // getting all posts
    public Iterable<Post> getPosts() {
        return postRepository.findAll();
    }

    // s05
    // updating a post
    public ResponseEntity updatePost(Long id, String stringToken, Post post) {
        // User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        Post postForUpdating = postRepository.findById(id).get();
        String postAuthor = postForUpdating.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if (authenticatedUser.equals(postAuthor)) {
            postForUpdating.setTitle(post.getTitle());
            postForUpdating.setContent(post.getContent());
            postRepository.save(postForUpdating);
            return new ResponseEntity<>("Post updated successfully", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are not authorized to edit this post", HttpStatus.UNAUTHORIZED);
        }
    }

    // deleting a post
    public ResponseEntity deletePost(Long id, String stringToken) {
        Post postForUpdating = postRepository.findById(id).get();
        String postAuthor = postForUpdating.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if (authenticatedUser.equals(postAuthor)) {
            postRepository.deleteById(id);
            return new ResponseEntity<>("Post deleted successfully", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are not authorized to delete this post", HttpStatus.UNAUTHORIZED);
        }
    }

    // s05 activity
    // getting user's posts
    public Iterable<Post> getUserPosts(String stringToken){
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
        User user = userRepository.findByUsername(authenticatedUser);

        return user.getPost();
    }

}